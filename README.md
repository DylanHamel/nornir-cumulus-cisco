# Nornir for Cumulus & Cisco

Automation with Nornir.

Example :

```shell
/Volumes/Data/gitlab/nornir-cumulus-cisco(master*) » ./bin/python main.py                                                      
napalm_get**********************************************************************
* backbone01.ch.gva.dc1 ** changed : False *************************************
vvvv napalm_get ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
{ 'facts': { 'fqdn': 'Backbone01.dh.local',
             'hostname': 'Backbone01',
             'interface_list': [ 'GigabitEthernet0/0',
                                 'GigabitEthernet0/1',
                                 'GigabitEthernet0/2',
                                 'GigabitEthernet0/3'],
             'model': 'IOSv',
             'os_version': 'IOSv Software (VIOS-ADVENTERPRISEK9-M), Version '
                           '15.6(1)T, RELEASE SOFTWARE (fc1)',
             'serial_number': '9RLHFILH4V8PSBHOUSJ93',
             'uptime': 2940,
             'vendor': 'Cisco'}}
^^^^ END napalm_get ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
remote_command******************************************************************
* spine01.ch.gva.dc1 ** changed : False ****************************************
vvvv remote_command ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

C>* 10.0.4.0/24 is directly connected, eth0, 00:00:20
C>* 10.10.0.101/32 is directly connected, lo, 00:00:20



show ipv6 route
===============
Codes: K - kernel route, C - connected, S - static, R - RIPng,
       O - OSPFv3, I - IS-IS, B - BGP, N - NHRP, T - Table,
       v - VNC, V - VNC-Direct, A - Babel, D - SHARP, F - PBR,
       > - selected route, * - FIB route

C>* fe80::/64 is directly connected, eth0, 00:00:20 

show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

C>* 10.0.4.0/24 is directly connected, eth0, 00:00:20
C>* 10.10.0.101/32 is directly connected, lo, 00:00:20



show ipv6 route
===============
Codes: K - kernel route, C - connected, S - static, R - RIPng,
       O - OSPFv3, I - IS-IS, B - BGP, N - NHRP, T - Table,
       v - VNC, V - VNC-Direct, A - Babel, D - SHARP, F - PBR,
       > - selected route, * - FIB route

C>* fe80::/64 is directly connected, eth0, 00:00:20 

^^^^ END remote_command ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* spine02.ch.gva.dc1 ** changed : False ****************************************
vvvv remote_command ** changed : False vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv INFO
show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

C>* 10.0.4.0/24 is directly connected, eth0, 00:00:02
C>* 10.10.0.102/32 is directly connected, lo, 00:00:02



show ipv6 route
===============
Codes: K - kernel route, C - connected, S - static, R - RIPng,
       O - OSPFv3, I - IS-IS, B - BGP, N - NHRP, T - Table,
       v - VNC, V - VNC-Direct, A - Babel, D - SHARP, F - PBR,
       > - selected route, * - FIB route

C>* fe80::/64 is directly connected, eth0, 00:00:02 

show ip route
=============
Codes: K - kernel route, C - connected, S - static, R - RIP,
       O - OSPF, I - IS-IS, B - BGP, E - EIGRP, N - NHRP,
       T - Table, v - VNC, V - VNC-Direct, A - Babel, D - SHARP,
       F - PBR,
       > - selected route, * - FIB route

C>* 10.0.4.0/24 is directly connected, eth0, 00:00:02
C>* 10.10.0.102/32 is directly connected, lo, 00:00:02



show ipv6 route
===============
Codes: K - kernel route, C - connected, S - static, R - RIPng,
       O - OSPFv3, I - IS-IS, B - BGP, N - NHRP, T - Table,
       v - VNC, V - VNC-Direct, A - Babel, D - SHARP, F - PBR,
       > - selected route, * - FIB route

C>* fe80::/64 is directly connected, eth0, 00:00:02 

^^^^ END remote_command ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
```