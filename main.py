#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
This script is a simple script for deploy Cumulus Linux and Cisco devices.

D1 => Deploy a network from scratch
D2 => Add modification to this network

/!\ /!\ /!\ /!\
Developpement and tests in progress !
/!\ /!\ /!\ /!\
"""


__author__ = "Dylan Hamel"
__version__ = "0.1"
__email__ = "dylan.hamel@protonmail.com"
__status__ = "Prototype"

######################################################
#
# Default value used for exit()
#
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

######################################################
#
# Import Library
#
try:
    import nornir.core
    from nornir import InitNornir
    from nornir.plugins.functions.text import print_result
    from nornir.plugins.tasks.networking import napalm_get
    from nornir.plugins.tasks import networking
    from nornir.plugins.tasks import commands
    from nornir.plugins.tasks.data import load_yaml
    from nornir.core.deserializer.inventory import InventoryElement
    from nornir.plugins.tasks.text import template_file
    from nornir.plugins.tasks.networking import napalm_configure
    from nornir.plugins.tasks import files
except ImportError as importError:
    print("Error import nornir")
    print(importError)
    exit(EXIT_FAILURE)



try:
    import datetime
except ImportError as importError:
    print("Error import datetime")
    print(importError)
    exit(EXIT_FAILURE)

try:
    import json
except ImportError as importError:
    print("Error import json")
    print(importError)
    exit(EXIT_FAILURE)

try:
    from netmiko import ConnectHandler
    from netmiko import file_transfer
except ImportError as importError:
    print("Error import netmiko")
    print(importError)
    exit(EXIT_FAILURE)

######################################################
#
# Constantes
#

######################################################
#
# Functions
#
def generate_interfaces(task):

    config = ""

    r1 = task.run(
        name="Loading Host specific datas",
        task=load_yaml,
        file=f"./data/parameters/{task.host}/host_interface.yml",
    )

    task.host["host_interface"] = r1.result

    r0 = task.run(
        name="Generate Cumulus interfaces configuration",
        task=template_file,
        template="interface.j2",
        path="./data/templates",
    )

    print_result("WRITE IN FILE", r0)

    f = open(
        f"./data/parameters/{task.host}/configs/interfaces_{task.host}_{datetime.date.today()}.cfg", "w+")
    f.write(r0.result)
    f.close()

    
# ========================================================================
#
#

######################################################
#
# MAIN Functions
#
def main():
    pass

######################################################
#
# TEST Functions
#
def test():

    nr = InitNornir(config_file="config.yml")
    
    # routers_gva = nr.filter(site="ch.gva.dc1", role="router")    
    # result = routers_gva.run(task=networking.napalm_get,getters=["facts"])
    # print_result(result)
    
    # spines_gva = nr.filter(site="ch.gva.dc1", role="spine")
    # re2 = spines_gva.run(task=commands.remote_command,command="net show lldp json")
    # print_result(re2)
    
    leaves_gva = nr.filter(site="ch.gva.dc1", role="leaf")
    
    #result = spines_gva.run(task=generate_interfaces, num_workers=1)
    print(leaves_gva.inventory.hosts)
    #result = leaves_gva.run(task=generate_interfaces, num_workers=1)
    #print_result(result)

    r1 = leaves_gva.run(files.sftp,
                        action="put",
                        src="/Volumes/Data/gitlab/python-eveng-api/backup/cumulus-spine-leaf.unl/Leaf01/interfaces",
                        dst="/etc/network/interfaces",
                        num_workers=1,)

    print_result(r1)

    print(f'Bye....')
    


if __name__ == "__main__":
    test()
